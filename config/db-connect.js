import mongoose from "mongoose";
import { configDb } from "./config-db.js";

export const connect = async function() {
    mongoose.connect(configDb.db.host, {
        dbName: configDb.db.name,
        readPreference: "primaryPreferred",
        useNewUrlParser: true,
        useUnifiedTopology: true
    }).then(() => {
        console.log('Connected to DB')
    }).catch(err => console.log(err));
}