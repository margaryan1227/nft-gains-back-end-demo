import mongoose from "mongoose";
const { Schema } = mongoose;

const assetSchema = new Schema({
    collectionId: {
      type: mongoose.Schema.Types.ObjectId
    },
    id: {
        type: Number
    },
    num_sales: {
        type: Number
    },
    background_color: {
        type: String
    },
    image_url: {
        type: String
    },
    image_preview_url: {
        type: String
    },
    image_thumbnail_url: {
        type: String
    },
    image_original_url: {
        type: String
    },
    animation_url: {
        type: String
    },
    animation_original_url: {
        type: String
    },
    name: {
        type: String
    },
    description: {
        type: String
    },
    external_link: {
        type: String
    },
    asset_contract: {
        type: Schema.Types.Mixed
    },
    permalink: {
        type: String
    },
    // collection: {
    //     type: Schema.Types.Mixed
    // },
    decimals: {
        type: Number
    },
    token_metadata: {
        type: String
    },
    is_nsfw: {
        type: Boolean
    },
    owner: {
        type: Schema.Types.Mixed
    },
    sell_orders: {
        type: String
    },
    seaport_sell_orders: {
        type: String
    },
    creator: {
        type: Schema.Types.Mixed
    },
    traits: {
        type: Schema.Types.Array
    },
    last_sale: {
        type: Schema.Types.Mixed
    },
    top_bid: {
        type: String
    },
    listing_date: {
        type: Date
    },
    is_presale: {
        type: Boolean
    },
    transfer_fee_payment_token: {
        type: String
    },
    transfer_fee: {
        type: String
    },
    token_id: {
        type: String
    }
});

export default mongoose.model("asset", assetSchema);
