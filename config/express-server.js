import express from "express";
import {configDb} from "./config-db.js";
const app = express();

app.listen(configDb.http.port, () => {
    console.log(`Listening on port ${configDb.http.port}!`)
});

export { app };