import schedule from 'node-schedule';
import upcoming from './models/upcoming.model.js';
import {jobManager} from "./utils/job.manager.js";
import { collectionStatsService } from "./services/collection.stats.service.js";
import moment from "moment";

export const scheduler = {
    importCollections() {
        schedule.scheduleJob('* * * * *',  () => {
            jobManager.importTrendingCollections();
            jobManager.getCollectionAssets();
        });
    },
    updateCollections() {
        schedule.scheduleJob('0/30 * * * ', async () => {
            await jobManager.updateCollections();
        });
    },
    removeUpcomingCollections() {
        schedule.scheduleJob('0 0 * * * ',async function() {
            await upcoming.deleteMany({
                publicSale: {
                    $lte: moment().format()
                }
            })
        })
    },
    getCollStats() {
        schedule.scheduleJob('0 0 * * * ',async () => {
            await collectionStatsService.saveCollStats();
        })
    },
    collectionFilter() {
        schedule.scheduleJob('0 0 * * * ', async () => {
            await jobManager.collectionFilter();
        })
    }
}
