import { connect } from "./config/db-connect.js";
import { app } from "./config/express-server.js";
import registerMiddlewares from "./middlewares/index.js";
import { statsService } from "./services/import.stats.service.js";
import { scheduler } from "./jobs.js";

async function main () {
    await connect();
    registerMiddlewares(app);
    await statsService.ensureStats();
}

scheduler.collectionFilter();
scheduler.getCollStats();
scheduler.importCollections();
scheduler.updateCollections();
scheduler.removeUpcomingCollections();

main();