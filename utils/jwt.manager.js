import jwt from 'jsonwebtoken';

export default  async function generateAccessToken (address) {
    return jwt.sign({id: address}, process.env.TOKEN_SECRET || "SecretOfToken")
}