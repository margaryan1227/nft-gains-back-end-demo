import collection from "../models/collection.model.js";
import collectionStats from "../models/collectionStats.model.js";
import upcoming from "../models/upcoming.model.js";

const getSingleCollection = async (id) => {
    return collection.findById(id);
}

const getSingleCollectionStats = async (id) => {
    return collectionStats.find({ collectionId: id } );
}

const getUpcomingCollections = async (name, page, limit) => {
    const list = await upcoming.aggregate([
        {
            $match: {
               "name": new RegExp(name, 'i')
            }
        },
        {
            $sort: {
                created_date: -1
            }
        },
        {
            $skip: (page - 1) * limit
        },
        {
            $limit: limit
        }
    ]);

    const size = await upcoming.countDocuments();

    return { list, size }
}

const getGallery = async (page, limit) => {
    const list = await collection.aggregate([
        {
            $match: {
                "active_status": true,
            }
        },
        {
            $sort: {
                created_date: -1
            }
        },
        {
            $skip: (page - 1) * limit
        },
        {
            $limit: limit
        }
    ]);

    const size = await collection.countDocuments({ active_status: true });

    return { list, size }
}

const getRanking = async (name, page, limit) => {
    const list = await collection.aggregate([
        {
            $match: {
                "active_status": true,
                "name": new RegExp(name, 'i')
            }
        },
        {
            $addFields: {
              favorite: false
            }
        },
        {
            $sort: {
                created_date: -1
            }
        },
        {
            $skip: (page - 1) * limit
        },
        {
            $limit: limit
        }
    ]);

    const size = await collection.countDocuments({ active_status: true, name: new RegExp(name, 'i') });

    return { list, size }
}

const getGalleryByFilters = async (name, page, limit) => {
    const list = await collection.aggregate([
        {
            $match: {
                "active_status": true,
                $or: [
                    { name: new RegExp( name, 'i' ) },
                    { description: new RegExp( name, 'i' ) }
                ]
            }
        },
        {
            $skip: (page - 1) * limit,
        },
        {
            $limit: limit,
        },
        {
            $sort: {created_date: 1}
        }
    ]);

    const size = await collection.countDocuments({ active_status: true, $or: [
            { name: new RegExp( name, 'i' ) },
            { description: new RegExp( name, 'i' ) },
        ]});

    return { list, size }
}

export const frontendService = {
    getSingleCollection,
    getSingleCollectionStats,
    getUpcomingCollections,
    getGallery,
    getRanking,
    getGalleryByFilters
}