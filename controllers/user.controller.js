import { userService } from "../services/user.service.js";
import generateAccessToken from "../utils/jwt.manager.js";

const addUser = async (req, res, next) => {
    try {
        const currentUser = await userService.getCurrentUser(req.body.address);
        const token = await generateAccessToken(req.body.address);

        if (currentUser) {
            res.send({
                success: true,
                token
            })
        } else {
          await userService.addUser(req.body.address);
            res.status(201).send({
                success: true,
                token
            })
        }
    } catch (e) {
        console.log(e)
    }
}

export const userController = {
    addUser
}