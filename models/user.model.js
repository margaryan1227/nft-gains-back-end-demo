import mongoose from 'mongoose';
const { Schema } = mongoose;

const userSchema = new Schema({
    address: {
        type: String,
        unique: true
    },

    favorites: {
        type: Array
    }
},{
    timestamps: true
});

export default mongoose.model('user', userSchema);