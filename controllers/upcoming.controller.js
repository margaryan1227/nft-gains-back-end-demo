import { upcomingService } from "../services/upcoming.service.js";

const getUpcoming = async (req, res, next) => {
    try {
        const list = await upcomingService.getUpcoming();

        res.send({
            success: true,
            data: list
        })
    } catch (e) {
        console.log(e)
        res.send({
            error: true,
            success: false,
            description: e.message
        })
    }
}

const deleteUpcoming = async (req, res, next) => {
    try {
        const list = await upcomingService.deleteUpcoming(req.params.id);

        res.send({
            success: true
        })
    } catch (e) {
        console.log(e)
        res.send({
            error: true,
            success: false,
            description: e.message
        })
    }
}

const addUpcoming = async (req, res, next) => {
    try {
        const control = await upcomingService.getSingleUpcoming(req.body.name);

        if (control.length > 0) {
             res.send({
                success: false,
                message: "Collection already exists"
            })
        } else {
            const newUpcoming = await upcomingService.addUpcoming(req.body);
            res.send({
                success: true,
                data: newUpcoming,
            })
        }
    } catch (e) {
        console.log(e);
        res.send({
            error: true,
            success: false,
            message: e.message
        });
    }
}

export const upcomingController = {
    getUpcoming,
    deleteUpcoming,
    addUpcoming
}