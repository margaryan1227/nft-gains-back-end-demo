import { collectionServices } from "../services/collection.service.js";
import jwt from "jsonwebtoken";


const getCollection = async (req, res, next) => {
    try {
    const offset = req.query.offset ? parseInt(req.query.offset) : 0;
    const limit = req.query.limit ? parseInt(req.query.limit) : 20;
    const { coll, list, size } = await collectionServices.getCollection(req.params.id, offset, limit);

    const collectionTraits = coll.traits;
        console.log(collectionTraits);

    res.status(200).send({
        success: true,
        collection: coll,
        assets: list,
        size: size
    });

    } catch (ex) {
        res.status(500).send({
            error: true,
            success: false,
            description: ex.message
        });
    }
};

const addCollectionToWatchlist = async (req, res, next) => {
    try {
        const token = req.headers['authorization'] && req.headers['authorization'].split(' ')[1];
        const currentUserAddress = jwt.decode(token);
        await collectionServices.addCollectionToWatchlist(req.params.id, currentUserAddress.id);

        res.status(201).send({
            success: true,
            message: "Favorite list changed successfully"
        })
    }catch (e) {
        console.log(e)

        res.status(404).send({
            success: false,
            message: "Can't find collection with such id."
        })
    }
}

const deleteCollection = async (req, res, next) => {
    try {
        await collectionServices.deleteCollection(req.params.id);

        res.status(200).send({
            success:true,
            message: "Deleted successfully."
        })
    } catch (e) {
        res.status(404).send({
            error: true,
            success: false,
            description: e.message
        });
        console.log(e);
    }
}

export const collectionController = {
    addCollectionToWatchlist,
    getCollection,
    deleteCollection
};