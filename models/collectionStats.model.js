import mongoose from 'mongoose';
const { Schema } = mongoose;

const collectionStatsSchema = new Schema({

    collectionId: {
        type: mongoose.Types.ObjectId
    },

    date: {
        type: Date
    },

    volume: {
        type: Number
    },

    change: {
        type: Number
    },

    sales: {
        type: Number
    },

    average_price: {
        type: Number
    }

})

export default  mongoose.model("collectionStats", collectionStatsSchema);