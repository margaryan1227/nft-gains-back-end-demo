import mongoose from 'mongoose';
const { Schema } = mongoose;


const SyncStatsModel = new Schema({
    latestImportDate: {
        type: Schema.Types.Date,
    },
    latestImportCount: {
        type: Schema.Types.Number,
    },
    latestUpdateDate: {
        type: Schema.Types.Date,
    },
    latestUpdateCount: {
        type: Schema.Types.Number,
    },
});

export default mongoose.model('syncStats', SyncStatsModel);