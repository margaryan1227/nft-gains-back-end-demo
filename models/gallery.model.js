import mongoose from 'mongoose';
const { Schema } = mongoose;

const GallerySchema = new Schema({
    collectionRef: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'collection',
        required: true
    },
    expirationDate: {
        type: Date,
    }
});

export default  mongoose.model('gallery', GallerySchema);