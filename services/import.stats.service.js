import syncStats from '../models/syncStats.model.js';

export const statsService = {
    ensureStats: async() => {
        let ss = await syncStats.findOne();
        if(ss == null) {
            ss = new syncStats();
            ss.latestImportCount = 0;
            ss.latestUpdateCount = 0;
            ss.save();
        }
    },

    updateImport: async(count) => {

        await syncStats.findOneAndUpdate({}, {
                latestImportDate: new Date(),
                latestImportCount: count
            }
        ).exec();
    },

    updateStats: async(count) => {
        await syncStats.findOneAndUpdate({}, {
                latestUpdateDate: new Date(),
                latestUpdateCount: count
            }
        ).exec();
    }
}