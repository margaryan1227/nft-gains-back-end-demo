import { Router } from "express";
import { userController } from "../controllers/user.controller.js";

const userRoute = Router();

userRoute.post("/user/sign-in", userController.addUser);

export { userRoute };