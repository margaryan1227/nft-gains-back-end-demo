import { frontendService } from "../services/frontend.service.js";
import { userService } from "../services/user.service.js";
import {collectionServices} from "../services/collection.service.js";

const getSingleCollection = async (req, res, next) => {
    try {
        const data = await frontendService.getSingleCollection(req.params.id);

        if (!data) {
            res.status(404).send({
                success: false,
                message: "No such collection"
            })
        }

        res.send({
            success: true,
            data
        })
    }catch (e) {
        console.log(e);
    }

}

const getSingleCollectionStats = async (req, res, next) => {
    try {
        const stats = await frontendService.getSingleCollectionStats(req.params.id);

        if (stats.length > 0) {
            res.send({
                success: true,
                size: stats.length,
                stats: stats
            })
        } else {
            res.status(404).send({
                success: false,
                message: "Analytics for this collection not available for now.",
                stats: [],
            })
        }

    } catch (e) {
        console.log(e)
    }
}

const getUpcomingCollections = async  (req, res, next) => {
    try {
        const page = req.query.page ? parseInt(req.query.page) : 1;
        const limit = req.query.limit ? parseInt(req.query.limit) : 30;
        let name = req.query.name;

        switch (name) {
            case "\\": {
                name = /\\/;
                break;
            }
            case "\s": {
                name = /\\s/;
                break;
            }
            case null || undefined: {
                name = " "
                break;
            }
        };
        const { list, size } = await frontendService.getUpcomingCollections(name, page, limit);

        if (list.length >= 1) {
            res.send({
                success: true,
                size: size,
                data: list
            })
        } else if (req.query.name) {
            res.status(404).send({
                success: false,
                data: [],
                message: "No upcoming collections with such name."
            });
        } else {
            res.status(404).send({
                success: false,
                data: [],
                message: "Upcoming list is empty."
            });
        }
    } catch (e) {
        console.log(e)
    }
}

const getGallery = async (req, res, next) => {
    try {
        const page = req.query.page ? parseInt(req.query.page) : 1;
        const limit = req.query.limit ? parseInt(req.query.limit) : 20;

        const { list, size } = await frontendService.getGallery(page, limit);
        if (page <= size / limit + 1) {
            res.send({
                success: true,
                size: size,
                data: list
            });
        } else {
            res.sendStatus(404);
        }
    } catch (e) {
        console.log(e)
    }
}

const getRanking = async (req, res, next) => {
    try {
        const page = req.query.page ? parseInt(req.query.page) : 1;
        const limit = req.query.limit ? parseInt(req.query.limit) : 30;
        let name = req.query.name;

        switch (name) {
            case "\\": {
                name = /\\/;
                break;
            }
            case "\s": {
                name = /\\s/;
                break;
            }
            case null || undefined: {
                name = " "
                break;
            }
        };

        const { list, size } = await frontendService.getRanking(name, page, limit);

        const data = [];

        for (const item of list) {
            const elem = {
                _id: item["_id"],
                name: item["name"],
                stats: item["stats"],
                img: item["image_url"],
                slug: item["slug"],
                favorite: item["favorite"]
            };
            data.push(elem);
        }

        const token = req.headers['authorization'] && req.headers['authorization'].split(' ')[1];

        if (token) {
            const currentUserAddress = jwt.decode(token);
            const currentUser = await userService.getCurrentUser(currentUserAddress.id);
            const collIdes = currentUser["favorites"];
            for (const elem of data) {
                for(let i = 0; i < collIdes.length; ++i) {
                    if (elem._id === collIdes[i]) {
                        elem.favorite = true;
                    }
                }
            }
        }

        if(data.length>0) {
            res.send({
                success: true,
                size: size,
                data: data
            })
        }else if (req.query.name) {
            res.send({
                success: false,
                message: "No collections with such name.",
                data: []
            })
        } else {
            res.send({
                success: false,
            })
        }

    } catch (e) {
        console.log(e)
    }
}

const getGalleryByFilter = async (req, res, next) => {
    try {
        if (req.params.filter === '3d' || req.params.filter === 'metaverse') {
            const page = req.query.page ? parseInt(req.query.page) : 1;
            const limit = req.query.limit ? parseInt(req.query.limit) : 20;

            const {list, size} = await frontendService.getGalleryByFilters(name, page, limit)

            res.send({
                success: true,
                size: size,
                data: list
            })
        } else {
            res.status(404).send({
                success: false,
                message: "Unavailable filters."
            })
        }
    } catch (e) {
        console.log(e)
    }
}

const getUserWatchlist = async (req, res, next) => {
    try {
        const watchlist = [];
        const token = req.headers['authorization'] && req.headers['authorization'].split(' ')[1];
        const currentUserAddress = jwt.decode(token);
        const currentUser = await userService.getCurrentUser(currentUserAddress.id);
        const collIdes = currentUser["favorites"];

        for(let i = 0; i < collIdes.length; i++) {
            const item = await frontendService.getSingleCollection(collIdes[i]);
            const elem = {
                _id: item["_id"],
                name: item["name"],
                stats: item["stats"],
                img: item["image_url"],
                slug: item["slug"]
            }
            watchlist.push(elem);
        }
        const page = req.query.page || 1;
        if(watchlist.length > 0) {
            res.send({
                success: true,
                size: watchlist.length,
                data: watchlist.slice((page-1)*20 , 20)
            })
        } else {
            res.send({
                success: false,
                message: "User don't have watchlist.",
                data: []
            })
        }
    } catch (e) {
        console.log(e)
    }
}

export const frontendController = {
    getSingleCollection,
    getUpcomingCollections,
    getSingleCollectionStats,
    getGallery,
    getRanking,
    getGalleryByFilter,
    getUserWatchlist
}