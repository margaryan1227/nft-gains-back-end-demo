import asset from "../models/asset.model.js";

const getSingleAsset = async function (id) {
    return asset.findById(id);
}

const getAssets = async function (id) {
    return asset.find({collectionId: id});
}

export const assetService = {
    getSingleAsset,
    getAssets
}