import {Router} from "express";
import {upcomingController} from "../controllers/upcoming.controller.js";

const upcomingRoute = Router();

upcomingRoute.get("/upcoming", upcomingController.getUpcoming);
upcomingRoute.delete("/upcoming/:id", upcomingController.deleteUpcoming);
upcomingRoute.post("/upcoming/add", upcomingController.addUpcoming);

export { upcomingRoute };