import upcoming from "../models/upcoming.model.js";

const getUpcoming = async () => {
    return upcoming.find().sort({ public_sale: -1 })
}

const deleteUpcoming = async (id) => {
    return upcoming.findByIdAndDelete(id);
}

const getSingleUpcoming = async (name) => {
    return upcoming.find({ name: name });
}

const addUpcoming = async (upcomingColl) => {
    return upcoming.create({
        name: upcomingColl.name,
        image: upcomingColl.image,
        twitterUrl: upcomingColl.twitter.url,
        twitterCount: upcomingColl.twitter.count,
        discordUrl: upcomingColl.discord.url,
        discordCount: upcomingColl.discord.count,
        supply: upcomingColl.supply,
        basePrice: upcomingColl.basePrice,
        publicSale: upcomingColl.publicSale
    })
}

export const upcomingService = {
    getUpcoming,
    deleteUpcoming,
    getSingleUpcoming,
    addUpcoming
}