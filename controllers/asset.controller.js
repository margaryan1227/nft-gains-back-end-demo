import { assetService } from "../services/asset.service.js";

const getSingleAsset = async function (req, res, next) {
    try {
        const data = await assetService.getSingleAsset(req.params.id);

        if (!data) {
            res.send({
                success: false,
                message: "Asset doesn't exist."
            })
        } else {
            res.send({
                success: true,
                data
            })
        }
    } catch (e) {
        console.log(e)
    }
}

export const assetController = {
    getSingleAsset
}