import defaultMiddleware from './default.js';

export default function (server) {
    defaultMiddleware(server);
}