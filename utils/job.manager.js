import axios from "axios";
import collection from '../models/collection.model.js';
import { statsService } from '../services/import.stats.service.js';
import moment from 'moment';
import gallery from '../models/gallery.model.js';
import throttle from 'promise-ratelimit';
import  asset  from '../models/asset.model.js';

export const jobManager = {
    importAssets: async function (slug, id) {
        try {

            const config = {
                headers: {
                    "X-API-KEY": process.env.OPENSEA_APIKEY
                }
            };

            const control = await asset.find({ collectionId: id });

            if(control.length > 0) {
                return null;
            }

            let res = await axios.get(`${process.env.OPENSEA_URL}/assets?collection_slug=${slug}&order_direction=desc&limit=200&include_orders=false`, config);

            let next = res.data["next"].split("=").join('%3D');

            while (next) {
                    for (const item of res.data["assets"]) {
                        let elem = new asset();

                        elem.collectionId = id;
                        elem.id = item.id;
                        elem.num_sales = item.num_sales;
                        elem.background_color = item.background_color;
                        elem.image_url = item.image_url;
                        elem.image_preview_url = item.image_preview_url;
                        elem.image_thumbnail_url = item.image_thumbnail_url;
                        elem.image_original_url = item.image_original_url;
                        elem.animation_url = item.animation_url;
                        elem.animation_original_url = item.animation_original_url;
                        elem.name = item.name;
                        elem.description = item.description;
                        elem.external_link = item.external_link;
                        elem.asset_contract = item.asset_contract;
                        elem.permalink = item.permalink;
                        // elem.collection = item.collection;
                        elem.decimals = item.decimals;
                        elem.token_metadata = item.token_metadata;
                        elem.is_nsfw = item.is_nsfw;
                        elem.owner = item.owner;
                        elem.sell_orders = item.sell_orders;
                        elem.seaport_sell_orders = item.seaport_sell_orders;
                        elem.creator = item.creator;
                        elem.traits = item.traits;
                        elem.last_sale = item.last_sale;
                        elem.top_bid = item.top_bid;
                        elem.listing_date = item.listing_date;
                        elem.is_presale = item.is_presale;
                        elem.transfer_fee_payment_token = item.transfer_fee_payment_token;
                        elem.transfer_fee = item.transfer_fee;
                        elem.token_id = item.token_id;

                        await elem.save();
                    }

                res = await axios.get(`${process.env.OPENSEA_URL}assets?collection_slug=${slug}&order_direction=desc&limit=200&cursor=${next}&include_orders=false`, config);
                next = res.data["next"].split("=").join('%3D');
            }
        } catch (e) {
            console.log('CATCH------->')
            console.log(e);
        }
    },

    getCollectionAssets: async function () {
        try {
            const colls = await collection.find({ active_status: true });

            for (const item of colls) {
                await this.importAssets(item.slug, item._id);
            }
        } catch (e) {
            console.log(e);
        }
    },

    getTrendingContracts: async function () {
        let options = {
            method: 'POST',
            url: `${process.env.ICYTOOLS_URL}`,
            headers: {
                'x-api-key': process.env.ICYTOOLS_APIKEY,
                'Content-Type': 'application/json'
            },
            data: '{"query":" query TrendingCollections {  contracts(orderBy: SALES, orderDirection: DESC) {   edges {    node {     address     tokenStandard    }   }  } } "}'
        };


        const { data } = await axios.request(options);
        return data.data.contracts.edges;

    },

    getContractDetails: async function (address) {
        const config = {
            headers: {
                "X-API-KEY": process.env.OPENSEA_APIKEY
            }
        }

        const response = await axios.get(`${process.env.OPENSEA_URL}asset/0xb47e3cd837ddf8e4c57f05d70ab865de6e193bbb/1/?include_orders=false`, config);

        return response.data;
    },

    importFromOpenSea: async function (slug, values) {
        const config = {
            headers: {
                "X-API-KEY": process.env.OPENSEA_APIKEY
            }
        }
        try {
        const list = await collection.find({slug: slug});

        if (list.length > 0) {
            throw new Error("Collection already exists");
        }


            const response = await axios.get(`${process.env.OPENSEA_URL}collection/${slug}`, config);
            const data = response.data.collection;
            let newCollection = new collection();

            if (values) {
                Object.keys(values).forEach(k => {
                    newCollection[k] = values[k];
                });
            }

            newCollection.banner_image_url = data.banner_image_url;
            newCollection.chat_url = data.chat_url;
            newCollection.created_date = data.created_date;
            newCollection.default_to_fiat = data.default_to_fiat;
            newCollection.description = data.description;
            newCollection.dev_buyer_fee_basis_points = data.dev_buyer_fee_basis_points;
            newCollection.dev_seller_fee_basis_points = data.dev_seller_fee_basis_points;
            newCollection.discord_url = data.discord_url;
            newCollection.display_data = data.display_data;
            newCollection.editors = data.editors;
            newCollection.external_url = data.external_url;
            newCollection.featured = data.featured;
            newCollection.featured_image_url = data.featured_image_url;
            newCollection.hidden = data.hidden;
            newCollection.image_url = data.image_url;
            newCollection.instagram_username = data.instagram_username;
            newCollection.is_subject_to_whitelist = data.is_subject_to_whitelist;
            newCollection.large_image_url = data.large_image_url;
            newCollection.medium_username = data.medium_username;
            newCollection.name = data.name;
            newCollection.only_proxied_transfers = data.only_proxied_transfers;
            newCollection.opensea_buyer_fee_basis_points = data.opensea_buyer_fee_basis_points;
            newCollection.opensea_seller_fee_basis_points = data.opensea_seller_fee_basis_points;
            newCollection.payment_tokens = data.payment_tokens;
            newCollection.payout_address = data.payout_address;
            newCollection.primary_asset_contracts = data.primary_asset_contracts;
            newCollection.require_email = data.require_email;
            newCollection.safelist_request_status = data.safelist_request_status;
            newCollection.short_description = data.short_description;
            newCollection.slug = data.slug;
            newCollection.stats = data.stats;
            newCollection.telegram_url = data.telegram_url;
            newCollection.traits = data.traits;
            newCollection.twitter_username = data.twitter_username;
            newCollection.wiki_urlonly_proxied_transfers = data.wiki_urlonly_proxied_transfers;
            await newCollection.save();

            return newCollection;
        } catch (e) {
            console.log(e);
        }
        return null;
    },

    importTrendingCollections: async function () {
        const contractsList = await this.getTrendingContracts();
        let importedCollections = [];
        let count = 0;

        for (let a = 0; a < contractsList.length; a++) {
            let check = await collection.find({"primary_asset_contracts.address": contractsList[a].node.address});
            if (check.length === 0) {
                await throttle(500)();
                const contractDetails = await this.getContractDetails(contractsList[a].node.address);
                try {
                    await throttle(500)();
                    const values = {last_trending: new Date()}
                    const finalCollections = await this.importFromOpenSea(contractDetails.collection.slug, values);
                    importedCollections.push(finalCollections);
                    ++count;
                } catch (e) {
                    console.log(e);
                }
            } else {
                check[0].last_trending = new Date();
                await check[0].save();
            }
        }

        if (count > 0) {
            await statsService.updateImport(count);
        }
        return importedCollections;
    },

    updateCollections: async function () {
        const config = {
            headers: {
                "X-API-KEY": process.env.OPENSEA_APIKEY
            }
        }
        let list = await collection.find();
        let count = 0;
        for (let a = 0; a < list.length; ++a) {
            await throttle(500)();
            const url = `${process.env.OPENSEA_URL}collection/${list[a].slug}/stats`;
            try {
                const stats = await axios.get(url, config);
                list[a].stats = stats.data.stats;
                await list[a].save();
                ++count;

            } catch (e) {
                console.log(e);
            }
        }
        if (count > 0) {
            await statsService.updateStats(count);
        }
        return list;
    },

    async getAutomaticGallery (filter) {
        const minDate = moment().subtract(1, 'months').toDate();
        const minTrendingDate = moment().subtract(7, 'days').toDate();
        filter = {
            created_date: {$gte: minDate},
            last_trending: {$gte: minTrendingDate},
            ...filter
        };

        return collection.find(filter);
    },

    async collectionFilter () {
        try {
            let manualList = await gallery.find().populate('collectionRef');
            manualList = Array.from(manualList);
            let automaticList = await this.getAutomaticGallery();
            let list = manualList.filter(item => item.collectionRef != null);
            list = list.map(item => item.collectionRef);

            for (const item of automaticList) {
                const control = list.find(item2 => item2.slug === item.slug)
                if (!control) {

                    await collection.updateOne({
                        slug: item["slug"]
                    },{
                        active_status: true
                    })
                }
            }

        } catch (e) {
            console.log(e)
        }
    }
}


