import collectionStats from "../models/collectionStats.model.js";
import collection from "../models/collection.model.js";
import moment from "moment";

export const collectionStatsService = {
    saveCollStats: async  () => {

        const galleryList = await collection.find({active_status: true});
        const isExist = await collectionStats.findOne({ day: {$lte: moment().subtract(30, 'days').format()}});

        for await (const galleryListElement of galleryList) {
            await collectionStats.updateOne({
                    collectionId: isExist._id,
                },
                {
                    date: moment().format(),
                    volume: galleryListElement.stats["one_day_volume"],
                    change: galleryListElement.stats["one_day_change"],
                    sales: galleryListElement.stats["one_day_sales"],
                    average_price: galleryListElement.stats["one_day_average_price"]
                })

            await collectionStats.create({
                collectionId: galleryListElement._id,
                date: moment().format(),
                volume: galleryListElement.stats["one_day_volume"],
                change: galleryListElement.stats["one_day_change"],
                sales: galleryListElement.stats["one_day_sales"],
                average_price: galleryListElement.stats["one_day_average_price"]
            })
        }
    }
}