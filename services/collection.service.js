import collection from "../models/collection.model.js";
import user from "../models/user.model.js";
import asset from "../models/asset.model.js";
import gallery from "../models/gallery.model.js";
import mongoose from "mongoose";

const getCollection = async (collId, offset, limit) => {
    collId = mongoose.Types.ObjectId(collId);
    const coll = await collection.findById(collId);
    const list = await asset.aggregate([
        {
            $match: {
                "collectionId": collId
            },
        },
        {
            $sort: {
                created_date: -1
            },
        },
        {
            $limit: limit
        },
        {
            $skip: offset
        }
    ])
    const size = await asset.countDocuments({ collectionId: collId });

    return { coll, list, size }
}

const addCollectionToWatchlist = async (collId, userId) => {
    try {
        if (collId) {
            const verify = await user.findOne({ favorites: collId })

            if (verify) {
                return user.updateOne({
                    address:userId,
                },{
                    $pullAll: {
                        "favorites": [collId]
                    }
                })
            } else {

                return user.updateOne({
                    address:userId,
                },{
                $push: {
                        "favorites": collId
                    }
                })
            }
        }

    } catch (e) {
        console.log(e)
    }
}

const deleteCollection = async (id) => {
    try {
            const control = await gallery.findOne({collectionRef: id});
        if (control) {
            await gallery.deleteOne({collectionRef: id});
        }
        await collection.deleteOne({_id: id});

    } catch (e) {
        console.log(e);
    }
}


export const collectionServices = {
    getCollection,
    addCollectionToWatchlist,
    deleteCollection
}