import { Router } from "express";
import { collectionController } from "../controllers/collection.controller.js";
import authToken from "../middlewares/authToken.js";

const collectionRoute = Router();

collectionRoute.get("/collection/:id", collectionController.getCollection);
collectionRoute.put("/collection/:id", authToken ,collectionController.addCollectionToWatchlist);
collectionRoute.delete("/collection/:id", collectionController.deleteCollection);

export { collectionRoute };


