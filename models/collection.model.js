import mongoose from 'mongoose';
const { Schema } = mongoose;

const CardDisplay = {
    card_display_style: {
        type: String,
    }
};

const CollectionSchema = new Schema({
    payment_tokens: [
        {
            type: Schema.Types.Array,
        }
    ],
    traits: {
        type: Schema.Types.Mixed,
    },
    stats: {
        type: Schema.Types.Mixed,
    },
    banner_image_url: {
        type: String,
    },
    created_date: {
        type: Date,
    },
    default_to_fiat: {
        type: Boolean,
    },
    description: {
        type: String,
    },
    dev_buyer_fee_basis_points: {
        type: Number,
    },
    dev_seller_fee_basis_points: {
        type: Number,
    },
    discord_url: {
        type: String,
    },
    display_data: { CardDisplay },
    external_url: {
        type: String,
    },
    featured: {
        type: Boolean,
    },
    featured_image_url: {
        type: String,
    },
    hidden: {
        type: Boolean,
    },
    safelist_request_status: {
        type: String,
    },
    image_url: {
        type: String,
    },
    is_subject_to_whitelist: {
        type: Boolean,
    },
    large_image_url: {
        type: String,
    },
    last_trending: {
        type: Date,
    },
    medium_username: {
        type: String
    },
    name: {
        type: String,
        unique: true
    },
    only_proxied_transfers: {
        type: Boolean
    },
    opensea_buyer_fee_basis_points: {
        type: Number,
    },
    opensea_seller_fee_basis_points: {
        type: Number,
    },
    payout_address: {
        type: String,
    },
    primary_asset_contracts: [
        {
            type: Schema.Types.Mixed,
        }
    ],
    require_email: {
        type: Boolean,
    },
    short_description: {
        type: String,
    },
    slug: {
        type: String,
    },
    telegram_url: {
        type: String,
    },
    twitter_username: {
        type: String,
    },
    instagram_username: {
        type: String,
    },
    wiki_url: {
        type: String,
    },
    active_status: {
        type: Boolean,
        default: false
    }
});

export default  mongoose.model('collection', CollectionSchema);