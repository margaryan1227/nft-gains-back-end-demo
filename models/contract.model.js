import mongoose from 'mongoose';
const { Schema } = mongoose;

const ContractAttribute = {
    name: {
        required: true,
        type: String,
    },
    value: {
        required: true,
        type: String,
    },
    valueCount: {
        required: true,
        type: Number,
    },
    rarity: {
        required: true,
        type: Number,
    },
}

const ContractSchema = new Schema({
    address: {
        required: true,
        type: String,
    },
    isVerified: {
        required: true,
        type: Boolean,
    },
    /*tokenStandard: TokenStandard,
    logs: LogConnection,
    token: Token,
    tokens: TokenConnection,*/
    tokenStandard: {
        required: true,
        type: String,
    },
    logs: {
        required: true,
        type: String,
    },
    token: {
        required: true,
        type: String,
    },
    tokens: {
        required: true,
        type: String,
    },
    attributes: [ContractAttribute]
});

export default mongoose.model('contract', ContractSchema);