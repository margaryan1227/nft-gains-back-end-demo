import morgan from "morgan";
import cors from "cors";
import bodyParser from "body-parser";
import dotenv from 'dotenv';
import fileUpload from 'express-fileupload';
import {routes} from "../routes/index.js";

export default server => {
    server.use(cors());
    server.use(morgan("dev"));
    dotenv.config();
    server.use(bodyParser.json());
    server.use(bodyParser.urlencoded());
    server.use(fileUpload({
        createParentPath: true
    }));
    server.use(routes);
}