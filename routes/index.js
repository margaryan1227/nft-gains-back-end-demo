import {userRoute} from "./user.route.js";
import {upcomingRoute} from "./upcoming.route.js";
import {frontendRoute} from "./frontend.route.js";
import {collectionRoute} from "./collection.route.js";
import {Router} from "express";
import assetRoute from "./asset.routes.js";

const routes = Router();

routes.use("/api", assetRoute);
routes.use("/api", userRoute);
routes.use("/api", upcomingRoute);
routes.use("/api", frontendRoute);
routes.use("/api", collectionRoute);

export {routes};