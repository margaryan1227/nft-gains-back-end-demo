import { Router } from "express";
import {assetController} from "../controllers/asset.controller.js";

const assetRoute = Router();

assetRoute.get("/asset/:id", assetController.getSingleAsset);

export default assetRoute;