import user from "../models/user.model.js";

const getCurrentUser = async (address) => {
    return user.findOne({ address: address  });
}

const addUser = async (address) => {
    return user.create({
        address: address
    })
}

export const userService = {
    getCurrentUser,
    addUser
}