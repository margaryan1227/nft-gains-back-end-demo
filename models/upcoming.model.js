import mongoose from 'mongoose';
const { Schema } = mongoose;

const UpcomingSchema = new Schema({
    name: {
        type: String,
    },
    image: {
        type: String,
    },
    twitterUrl: {
        type: String,
    },
    twitterCount: {
        type: Number,
    },
    discordUrl: {
        type: String,
    },
    discordCount: {
        type: Number,
    },
    supply: {
        type: Number,
    },
    basePrice: {
        type: Number,
    },
    publicSale: {
        type: Date,
    }
});
UpcomingSchema.set('toJSON', {
    virtuals: true,
    transform: function(doc, ret) {
        // delete ret._id;
        delete ret.id;
    }
});
export default mongoose.model('upcoming', UpcomingSchema);
