import { Router } from "express";
import authToken from "../middlewares/authToken.js";
import { frontendController } from "../controllers/frontend.controller.js";

const frontendRoute = Router();

frontendRoute.get("/frontend/collection/:id", frontendController.getSingleCollection);
frontendRoute.get("/frontend/collection/:id/analytics", frontendController.getSingleCollectionStats);
frontendRoute.get("/frontend/upcoming", frontendController.getUpcomingCollections);
frontendRoute.get("/frontend/gallery", frontendController.getGallery);
frontendRoute.get("/frontend/ranking", frontendController.getRanking);
frontendRoute.get("/frontend/gallery/:filter", frontendController.getGalleryByFilter);
frontendRoute.get("/frontend/watchlist", authToken, frontendController.getUserWatchlist);

export { frontendRoute }