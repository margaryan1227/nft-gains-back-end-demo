export const configDb =  {
    db: {
        host: process.env.DB_HOST || "mongodb+srv://dev123:dev123@cluster0.6lxrq.mongodb.net/?retryWrites=true&w=majority",
        name: process.env.DB_MONGO_NAME || 'nft-gains'
    },
    http: {
        port: process.env.PORT || 8081
    },
}
